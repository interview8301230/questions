class Node {
  constructor(data, next = null) {
    this.data = data;
    this.next = next;
  }
}

class LinkedList {
  // Do not add anything here
  constructor() {
    this.head = null;
  }

  // implement this method to insert a new Node at the begining of the linked list
  insertFirst(data) {
  }

  // implement this method to remove the the first node of the linked list
  removeFirst() {
  }

  // implement this method to insert a new Node at the end of the linked list
  insertLast(data) {
  }

  // implement this method to remove the the last node of the linked list
  removeLast() {
  }

  size() {
    let counter = 0;
    let node = this.head;
    while (node) {
      counter++;
      node = node.next;
    }
    return counter;
  }

  // impelement this method to return the Node which is n space behind, from the last Node
  // eg. if there are 8 Nodes in the linked list and n equals 3, this method should return fifth Node (8 - 3 = 5)
  // note that using the size method or/and using counter variable are not allowed
  fromLast(n) {
  }

  // impelement this method which makes the the linked list class an iterable,
  // so it will be possible to loop through the linked list instance using (for of)
  *[Symbol.iterator]() {
  }
}
