const EXTERNAL_DATA = [
  {
    title: "AAA",
  },
  {
    title: "BBB",
  },
  {
    title: "CCC",
  },
];

enum ELEMENT {
  LIST = "list",
  BUTTON = "button",
  TEXT = "text",
}

abstract class TAG<T extends HTMLElement> {
  protected _elm: T;
  protected dependency = new Map<ELEMENT, TAG<HTMLElement>>();
  constructor(elm: T) {
    this._elm = elm;
  }

  abstract handler(e: Event): void;

  abstract setData(param: any): void;

  setDependency<U extends HTMLElement>(name: ELEMENT, tag: TAG<U>) {
    this.dependency.set(name, tag);
  }
}

class SaveBtn extends TAG<HTMLButtonElement> {
  constructor(elm: HTMLButtonElement) {
    super(elm);
    this._elm.onclick = this.handler;
  }

  setData(value: boolean) {
    this._elm.disabled = value;
  }

  handler = (_e: Event) => {
    alert("SUCCESS");
  };
}

class TextInp extends TAG<HTMLInputElement> {
  constructor(elm: HTMLInputElement) {
    super(elm);
    this._elm.oninput = this.handler;
  }

  setData(value: string) {
    this._elm.value = value;
  }

  handler = (e: Event) => {
    const target = e.target as HTMLInputElement;
    (this.dependency.get(ELEMENT.LIST) as List).setData(target.value);
    let isBtnDisabled = true;
    EXTERNAL_DATA.forEach((itm) => {
      if (itm.title === target.value) {
        isBtnDisabled = false;
      }
    });

    (this.dependency.get(ELEMENT.BUTTON) as SaveBtn).setData(isBtnDisabled);
  };
}

class List extends TAG<HTMLUListElement> {
  constructor(elm: HTMLUListElement) {
    super(elm);
    for (let data of EXTERNAL_DATA) {
      const li = document.createElement("li");
      li.id = data.title;
      li.innerHTML = data.title;
      li.onclick = this.handler;
      this._elm.appendChild(li);
    }
  }

  setData = (value: string) => {
    for (let li of this._elm.children) {
      if (li.id === value) {
        li.classList.add("active");
      } else {
        li.classList.remove("active");
      }
    }
  };

  handler = (e: Event) => {
    const target = e.target as HTMLLIElement;
    this.setData(target.id);
    (this.dependency.get(ELEMENT.TEXT) as TextInp).setData(target.id);
    (this.dependency.get(ELEMENT.BUTTON) as SaveBtn).setData(false);
  };
}

const listUl = document.getElementById("list") as HTMLUListElement;
const txtInput = document.getElementById("text") as HTMLInputElement;
const saveButton = document.getElementById("save") as HTMLButtonElement;

const txtInp = new TextInp(txtInput);
const list = new List(listUl);
const saveBtn = new SaveBtn(saveButton);

list.setDependency(ELEMENT.TEXT, txtInp);
list.setDependency(ELEMENT.BUTTON, saveBtn);
txtInp.setDependency(ELEMENT.LIST, list);
txtInp.setDependency(ELEMENT.BUTTON, saveBtn);
